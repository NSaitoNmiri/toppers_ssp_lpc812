/*
 *  TOPPERS/SSP Kernel
 *      Smallest Set Profile Kernel
 * 
 *  Copyright (C) 2013 by Naoki Saito
 *             Nagoya Municipal Industrial Research Institute, JAPAN
 * 
 *  上記著作権者は，Free Software Foundation によって公表されている
 *  GNU General Public License の Version 2 に記述されている条件か，以
 *  下の(1)〜(4)の条件を満たす場合に限り，本ソフトウェア（本ソフトウェ
 *  アを改変したものを含む．以下同じ）を使用・複製・改変・再配布（以下，
 *  利用と呼ぶ）することを無償で許諾する．
 *  (1) 本ソフトウェアをソースコードの形で利用する場合には，上記の著作
 *      権表示，この利用条件および下記の無保証規定が，そのままの形でソー
 *      スコード中に含まれていること．
 *  (2) 本ソフトウェアを再利用可能なバイナリコード（リロケータブルオブ
 *      ジェクトファイルやライブラリなど）の形で利用する場合には，利用
 *      に伴うドキュメント（利用者マニュアルなど）に，上記の著作権表示，
 *      この利用条件および下記の無保証規定を掲載すること．
 *  (3) 本ソフトウェアを再利用不可能なバイナリコードの形または機器に組
 *      み込んだ形で利用する場合には，次のいずれかの条件を満たすこと．
 *    (a) 利用に伴うドキュメント（利用者マニュアルなど）に，上記の著作
 *        権表示，この利用条件および下記の無保証規定を掲載すること．
 *    (b) 利用の形態を，別に定める方法によって，上記著作権者に報告する
 *        こと．
 *  (4) 本ソフトウェアの利用により直接的または間接的に生じるいかなる損
 *      害からも，上記著作権者を免責すること．
 * 
 *  本ソフトウェアは，無保証で提供されているものである．上記著作権者は，
 *  本ソフトウェアに関して，その適用可能性も含めて，いかなる保証も行わ
 *  ない．また，本ソフトウェアの利用により直接的または間接的に生じたい
 *  かなる損害に関しても，その責任を負わない．
 * 
 */

/*
 *  ターゲットボードに関する定義(LPCXPRESSO-LPC812用)
 *
 *  カスタマイズ箇所：
 *  (1) PLLを使用しない場合は USE_SYSTEM_PLL マクロ定義をコメントアウトする(その場合メインクロックはIRCとなる)
 *  (2) 外部オシレータを使用しない場合は，USE_EXTERNAL_OSCILLATOR マクロ定義をコメントアウトする
 *  (3) オシレータの周波数が異なる場合は OSC_CLOCK の値を変更する
 *  (4) 生成したいメインクロックの周波数を変更する場合は MAIN_CLOCK の値を変更する
 *      その際，SYSPLLCTRLレジスタの設定値を TVALUE_SYSCON_SYSPLLCTRL マクロに指定する
 *      システムクロックの値を決定するため，メインクロックの分周比を TVALUE_SYSCON_SYSAHBCLKDIV に定義する
 *      (システムクロックは上限30MHzという制限がある)
 */

#ifndef TOPPERS_LPCXPRESSO_LPC812_H
#define TOPPERS_LPCXPRESSO_LPC812_H

#include <sil.h>
#include "LPC812M101FDH20.h"

// PLL を使用する（使用しない場合はコメントアウトする．）
// PLL不使用の場合，クロック源はIRCとなる(外部オシレーターは使用しない)．
#define USE_SYSTEM_PLL

// 外部オシレータを使用する（使用しない場合はコメントアウトする．クロック源はIRCとなる）
#define USE_EXTERNAL_OSCILLATOR

/*
 *  クロック関連の定義
 */
// 内部RC(IRC)クロックの周波数
#define IRC_CLOCK	(12 * 1000 * 1000)	/* 12MHz */

// オシレータクロックの周波数(IRCを使うときは IRC_CLOCK, 外部XTALを使用するときは実際の値に合わせて変更する)
#define OSC_CLOCK	(12 * 1000 * 1000)	/* 12MHz */

// メインクロック
#ifdef USE_SYSTEM_PLL
	#define MAIN_CLOCK	(60 * 1000 * 1000)	/* 60MHz */
#else
	#define MAIN_CLOCK	IRC_CLOCK	/* 12MHz */
#endif

/*
 * システムPLLの定義
 */
#ifdef USE_SYSTEM_PLL
	#ifdef USE_EXTERNAL_OSCILLATOR /* 外部クロックを使用 */
		// システムPLLのクロック源
		#define TVALUE_SYSCON_SYSPLLCLKSEL TBITPTN_SYSCON_SYSPLLCLKSEL_SEL_SYSOSC 	// クロック源としてシステムオシレーターを選択
		// システムオシレーターの設定
		#define TVALUE_SYSCON_SYSOSCCTRL	0	// システムオシレーターをバイパスしない(BYPASS=0), 入力クロックは 1-20MHzの範囲
	#else /* USE_EXTERNAL_OSCILLATOR */
		// システムPLLのクロック源
		#define TVALUE_SYSCON_SYSPLLCLKSEL TBITPTN_SYSCON_SYSPLLCLKSEL_SEL_IRC 	// クロック源として内部RCオシレーターを選択
	#endif /* USE_EXTERNAL_OSCILLATOR */

	// PLLの逓倍器および分周器のパラメータ
	// 以下からダウンロードできるPLLパラメータ設定用Excelシートで算出した．
	// http://www.lpcware.com/content/nxpfile/lpc8xx-clock-and-pll-configuration-tool
	// これによると，入力周波数が12MHzで60MHzのメインクロックを生成する場合，PSEL=2, MSEL=4
	#define TVALUE_SYSCON_SYSPLLCTRL_MSEL	(0x04)	// MSEL = 4
	#define TVALUE_SYSCON_SYSPLLCTRL	(TBITPTN_SYSCON_SYSPLLCTRL_PSEL_P2 | TVALUE_SYSCON_SYSPLLCTRL_MSEL)
#endif	/* USE_SYSTEM_PLL */

/*
 * メインクロックの定義
 */
#ifdef USE_SYSTEM_PLL
	#define TVALUE_SYSCON_MAINCLKSEL	TBITPTN_SYSCON_MAINCLKSEL_SEL_PLLOUT	// メインクロックはシステムPLLの出力クロックを使う
#else
	#define TVALUE_SYSCON_MAINCLKSEL	TBITPTN_SYSCON_MAINCLKSEL_SEL_IRC	// メインクロックはIRCを使う
#endif

/*
 * システムクロックの定義
 */
#ifdef USE_SYSTEM_PLL
	#define TVALUE_SYSCON_SYSAHBCLKDIV 2	// システムクロックはメインクロックを2分周(30MHz)
#else
	#define TVALUE_SYSCON_SYSAHBCLKDIV 1	// システムクロックはメインクロックを1分周(12MHz)
#endif

#define SYSTEM_CLOCK 	(MAIN_CLOCK/TVALUE_SYSCON_SYSAHBCLKDIV)

// 他モジュールへのクロック供給ON/OFF
#define TVALUE_SYSCON_SYSAHBCLKCTRL	(TBITPTN_SYSCON_SYSAHBCLKCTRL_SWM | TBITPTN_SYSCON_SYSAHBCLKCTRL_IOCON)	// SWM, IOCONのクロックON

/*
 * UARTのクロック定義
 */
#define TVALUE_SYSCON_UARTCLKDIV	(1)	// UARTのクロックはメインクロックを1分周

// BRGレジスタの設定値．"baudrate = (MAIN_CLOCK/UARTCLKDIV) / 16*(BRG+1)" の関係を満たすようにBRGを設定する．
#define TVALUE_USART_BRG(baudrate)	(MAIN_CLOCK/TVALUE_SYSCON_UARTCLKDIV/16/(baudrate) - 1)

// Fractional rate generator.
// ・ボーレートの目標値を goal_baudrate
// ・BRG の設定値から導出されるボーレート． real_baudrate = (main_clock/UARTCLKDIV) / 16*(BRG+1)
// ・DIV を UARTFRGDIVの設定値(データシートより，255固定)
// ・MULT を UARTFRGMULTの設定値(0-255)
// とするとき，次の関係を満たすように，MULTを設定してボーレートクロックを調整する
// {(MULT+1)/(DIV+1)} + 1 = real_baudrate / goal_baudrate
// 最初に決定したgoal_baudrateに基づきBRGの設定値を計算したら，DIVが255としてMULTの値を計算する．
#define TVALUE_SYSCON_UARTFRGDIV	(0xff)
#define TVALUE_SYSCON_UARTFRGMULT(baudrate) \
		((((MAIN_CLOCK / TVALUE_SYSCON_UARTCLKDIV) / 16) * (TVALUE_SYSCON_UARTFRGDIV + 1)) / (baudrate * (TVALUE_USART_BRG(baudrate) + 1))) - (TVALUE_SYSCON_UARTFRGDIV + 1)

// Systick
#define SYSTIC_CLOCK	(SYSTEM_CLOCK)


#ifndef TOPPERS_MACRO_ONLY

#endif /* TOPPERS_MACRO_ONLY */
#endif /* TOPPERS_LPCXPRESSO_LPC812_H */
